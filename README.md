This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

First dependency needed: [NodeJS](https://nodejs.org/)

Run `npm install` to install dependencies.
Run `npm start` to run the application.
Run `npm test` to run unit tests.

Issue: it was required to show the city details, but the only details I showed was the name (I just received name, coords and an id).

The file `src/cityList.json` is created by the json file provided from `openweathermap.org` and filtered by the country code (param deleted in the result json file because there it's a constant). The list in that file is used to select the city.

Unit test developed with [Jasmine](https://jasmine.github.io/) and [enzyme](https://github.com/airbnb/enzyme) to mock the inner component. The ajax request are managed with [Axios](https://github.com/mzabriskie/axios).
