import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App/App';
import registerServiceWorker from './registerServiceWorker';
import './index.css';
import * as cityList from './cityList.json';

ReactDOM.render(<App cityList={cityList} />, document.getElementById('root'));
registerServiceWorker();
