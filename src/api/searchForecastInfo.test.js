import searchForecastinfo from './searchForecastInfo';
import axios from 'axios';
import * as config from './api-config.json';

describe('searchForecastInfo service', () => {
    const cityId = 12;
    let getRequestResolveFn,
        getRequestRejectFn;

    beforeEach(() => {
        spyOn(axios, 'get').and.returnValue(new Promise ((resolveFn, rejectFn) => {
            getRequestResolveFn = resolveFn;
            getRequestRejectFn = rejectFn;
        }));
    });
    
    it('should execute the correct call', () => {
        searchForecastinfo.byCityId(cityId);
        expect(axios.get).toHaveBeenCalledWith(`http://api.openweathermap.org/data/2.5/forecast?id=12&appid=${config.key}`);
    });

    it('should compile correctly the response', (done) => {
        const apiMockResponse = {
            data: {
                list: [{
                    dt_txt: '2017-06-10 09:00:00',
                    main: {
                        temp: 300
                    }
                }, {
                    dt_txt: '2017-06-10 12:00:00',
                    main: {
                        temp: 320
                    }
                }, {
                    dt_txt: '2017-06-11 09:00:00',
                    main: {
                        temp: 290
                    }
                }]
            }
        };

        const expectedResult = [{
            date: '2017-06-10',
            forecast: [{
                dt_txt: '2017-06-10 09:00:00',
                time: '09:00',
                main: {
                    temp: 300,
                    celsiusTemp: 27
                }
            }, {
                dt_txt: '2017-06-10 12:00:00',
                time: '12:00',
                main: {
                    temp: 320,
                    celsiusTemp: 47
                }
            }]
        }, {
            date: '2017-06-11',
            forecast: [{
                dt_txt: '2017-06-11 09:00:00',
                time: '09:00',
                main: {
                    temp: 290,
                    celsiusTemp: 17
                }
            }]
        }];

        searchForecastinfo.byCityId(cityId).then((response)=>{
            expect(response).toEqual(expectedResult);
            done();
        });
        getRequestResolveFn(apiMockResponse);
    });
    
});
