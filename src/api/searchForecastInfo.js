import axios from 'axios';
import * as config from './api-config.json';
import _ from 'lodash';

const searchForecastInfo = {
    byCityId: searchForecastByCityId
};

export default searchForecastInfo;

function searchForecastByCityId (cityId) {
    const apiPath = `${config.path}?id=${cityId}&appid=${config.key}`;
    return axios.get(apiPath).then(response => {
        const slotList = response.data.list;
        let daySlots;
        
        slotList.forEach((slot) => {
            const completeTime = slot.dt_txt.split(' ')[1];
            const celsiusTemp = slot.main.temp - 273.15;
            slot.time = completeTime.substr(0, 5);
            slot.main.celsiusTemp = Math.round(celsiusTemp);
        });
        
        daySlots = _.groupBy(slotList, slot => slot.dt_txt.split(' ')[0]);

        return _.map(daySlots, (slotGroup, day) => ({
            date: day,
            forecast: slotGroup
        }));
    });
}
