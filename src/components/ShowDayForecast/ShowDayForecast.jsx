import React, { Component } from 'react';
import './ShowDayForecast.css';
import * as imgInfo from './api-info.json';

class ShowDayForecast extends Component {
    render () {
        function getIcon (slelectedForecast) {
            const code = slelectedForecast.weather[0].icon;

            return `${imgInfo.path}/${code}.png`;
        }
        return <span className="show-day-forecast">
                <h3>{this.props.dayForecast.date}</h3>
                <ul className="list-unstyled">
                        {this.props.dayForecast.forecast.map((forecast, forecastId) => <li key={forecastId} className="forecast-time-slot text-center">
                                <div className="slot-time"> {forecast.time} </div>
                                <img src={getIcon(forecast)} alt={forecast.weather[0].description} />
                                <div>
                                    <span className="slot-temperature">{forecast.main.celsiusTemp}</span>
                                    &#8451;
                                </div>
                            </li>)}
                    </ul>
            </span>;
    }
}

export default ShowDayForecast;
