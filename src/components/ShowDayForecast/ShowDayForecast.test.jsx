import React from 'react';
import ShowDayForecast from './ShowDayForecast';
import { shallow } from 'enzyme';

describe('<ShowDayForecast />', () => {
    it('should renders without crashing', () => {
        let dayForecast = {
            date: '',
            forecast: []
        };
        shallow(<ShowDayForecast dayForecast={dayForecast} />);
    });

    it('should show the date', () => {
        let dayForecast = {
            date: '2017-06-10',
            forecast: []
        };
        const component = shallow(<ShowDayForecast dayForecast={dayForecast} />);
        expect(component.find('h3').text()).toBe(dayForecast.date);
    });

    it('should show the forecast slots', () => {
        let dayForecast = {
            date: '',
            forecast: [{
                time: '12:00',
                main: {
                    celsiusTemp: 12
                },
                weather: [{
                    icon: 'a1',
                    description: '1st desc'
                }]
            }, {
                time: '15:00',
                main: {
                    celsiusTemp: 13
                },
                weather: [{
                    icon: 'a2',
                    description: '2nd desc'
                }]
            }, {
                time: '18:00',
                main: {
                    celsiusTemp: 10
                },
                weather: [{
                    icon: 'a3',
                    description: '3rd desc'
                }]
            }]
        };
        const component = shallow(<ShowDayForecast dayForecast={dayForecast} />);
        const forecastTimeSlots = component.find('.forecast-time-slot');
        expect(forecastTimeSlots.length).toBe(3);
        forecastTimeSlots.nodes.forEach((node, nodeIndex) => {
            let nodeComponent = shallow(node);
            let weatherIcon = nodeComponent.find('img');
            expect(nodeComponent.find('.slot-time').text()).toContain(dayForecast.forecast[nodeIndex].time);
            expect(nodeComponent.find('.slot-temperature').text()).toBe('' + dayForecast.forecast[nodeIndex].main.celsiusTemp);
            expect(weatherIcon.props().src).toContain(dayForecast.forecast[nodeIndex].weather[0].icon);
            expect(weatherIcon.props().alt).toBe(dayForecast.forecast[nodeIndex].weather[0].description);
        });
        
    });
});
