import React, { Component } from 'react';
import './App.css';
import forecastApi from '../../api/searchForecastInfo';

import SearchCity from '../SearchCity/SearchCity.jsx';
import ShowForecast from '../ShowForecast/ShowForecast.jsx';

class App extends Component {
  constructor (props) {
    super(props);
    this.state = {
      selectedCity: this.getCityOfLondon(),
      mode: 'show-forecast',
      forecast: []
    };
    this.updateForecast();
  }
  getCityOfLondon () {
    return this.props.cityList
      .filter(city => city.name.toLowerCase() === 'london')[0];
  }
  updateForecast () {
      forecastApi.byCityId(this.state.selectedCity.id).then(response => {
          this.setState({
              forecast: response
          });
      });
  }
  searchForCity () {
    this.setState({
      mode: 'select-city',
      forecast: []
    });
  }
  selectCity (city) {
    this.setState({
      selectedCity: city,
      mode: 'show-forecast'
    });
    this.updateForecast();
  }
  render() {
    return (<div className="app">
      {this.state.mode === 'show-forecast' ?
        <ShowForecast chooseCity={this.searchForCity.bind(this)} city={this.state.selectedCity} forecast={this.state.forecast}></ShowForecast> :
        <SearchCity cityList={this.props.cityList} selectCity={this.selectCity.bind(this)}></SearchCity>}
    </div>);
  }
}

export default App;
