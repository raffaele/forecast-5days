import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import { shallow } from 'enzyme';
import searchForecast from '../../api/searchForecastInfo';

import SearchCity from '../SearchCity/SearchCity.jsx';
import ShowForecast from '../ShowForecast/ShowForecast.jsx';

describe('<App />', () => {
  let cities = [{
    name: 'London'
  }];
  let resolveFn, rejectFn;
  beforeEach(() => {
    spyOn(searchForecast, 'byCityId').and.returnValue(new Promise ((resolveCallback, rejectCallback) => {
      resolveFn = resolveCallback;
      rejectFn = rejectCallback;
    }));
  });

  it('should renders without crashing', () => {
    shallow(<App cityList={cities} />);
  });

  it('should have the default state when it start', () => {
    const component = shallow(<App cityList={cities} />);
    expect(component.state()).toEqual({
      selectedCity: cities[0],
      mode: 'show-forecast',
      forecast: []
    });
  });

  it('should show the ShowForecast component by default and should pass the selected city as props', () => {
    const component = shallow(<App cityList={cities} />);
    const showForecastWrapper = component.find(ShowForecast);

    expect(showForecastWrapper.length).toBe(1);
    expect(showForecastWrapper.props().city).toBe(cities[0]);
  });

  it('should update the forecast', (done) => {
    const component = shallow(<App cityList={cities} />);
    let forecastResponse = [{}, {}];

    resolveFn(forecastResponse);
    setTimeout(() => {
      const showForecastWrapper = component.find(ShowForecast);
      expect(component.state().forecast).toBe(forecastResponse);
      expect(showForecastWrapper.props().forecast).toBe(forecastResponse);
      done();
    });
  });

  it('should pass in search-city mode', () => {
    const component = shallow(<App cityList={cities} />);
    component.instance().searchForCity();
    expect(component.find(ShowForecast).length).toBe(0);
    expect(component.find(SearchCity).length).toBe(1);
  });
});
