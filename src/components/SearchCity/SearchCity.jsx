import React, { Component } from 'react';
import { ListGroup } from 'react-bootstrap';

class SearchCity extends Component {
  constructor (props) {
    super(props);
    this.state = {
      selectedCityList: this.getCitySublist()
    };
  }
  getCitySublist (key = '') {
    return this.props.cityList.filter(city =>
      city.name.toLowerCase().startsWith(key.toLowerCase())
    ).slice(0, 10);
  }
  selectCityGroup (event) {
    this.setState({
      selectedCityList: this.getCitySublist(event.target.value)
    });
  }
  selectCity (city) {
    if (this.props.selectCity) {
      this.props.selectCity(city);
    }
  }
  render() {
    return (
      <div className="search-city container">
        <div>
          <input className="search-city__input-field" type="text" placeholder="search UK city" onChange={this.selectCityGroup.bind(this)}/>
        </div>
        <ListGroup componentClass="ul">
          { this.state.selectedCityList.map((city, cityIndex) =>
                <li key={cityIndex} className="list-group-item" onClick={this.selectCity.bind(this, city)}>{city.name}</li>)
          }
        </ListGroup>
      </div>
    );
  }
}

export default SearchCity;