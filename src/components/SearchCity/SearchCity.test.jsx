import React from 'react';
import SearchCity from './SearchCity';
import { shallow } from 'enzyme';

describe('<SearchCity />', () => {
    let cityList = [];

    it('should renders without crashing', () => {
        shallow(<SearchCity cityList={cityList} />);
    });

    it('should show all the cities in the list', () => {
        cityList = [{
            name: 'London'
        }, {
            name: 'Manchester'
        }, {
            name: 'Glasgow'
        }];
        const component = shallow(<SearchCity cityList={cityList} />);
        const showedCityList = component.find('.list-group-item');

        expect(showedCityList.length).toBe(3);
    });

    it('should show just the filtered cities in the list', () => {
        cityList = [{
            name: 'London'
        }, {
            name: 'Manchester'
        }, {
            name: 'Glasgow'
        }];
        const component = shallow(<SearchCity cityList={cityList} />);
        component.find('.search-city__input-field').simulate('change', {
            target: {
                value: 'lon'
            }
        });
        const showedCityList = component.find('.list-group-item');
        expect(showedCityList.length).toBe(1);
        expect(showedCityList.text()).toBe('London');
    });

    it('should call the callback on city click', () => {
        cityList = [{
            name: 'London'
        }];
        const selectCity = jasmine.createSpy();
        const component = shallow(<SearchCity cityList={cityList} selectCity={selectCity} />);
        component.find('.list-group-item').simulate('click');
        expect(selectCity).toHaveBeenCalledWith(cityList[0]);
    });
});
