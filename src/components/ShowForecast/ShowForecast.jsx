import React, { Component } from 'react';
import { ListGroup } from 'react-bootstrap';
import ShowDayForecast from '../ShowDayForecast/ShowDayForecast';

class ShowForecast extends Component {
    chooseCity () {
        if (this.props.chooseCity) {
            this.props.chooseCity();
        }
    }
    render() {
        return (<div className="show-forecast">
            <header className="app__header">
                {this.props.city.name}
                <span className="app__header__search-icon glyphicon glyphicon-search" onClick={this.chooseCity.bind(this)}></span>
            </header>
            
            <ListGroup componentClass="ul">
                {this.props.forecast.map(slot => <li key={slot.date} className="list-group-item">
                    <ShowDayForecast dayForecast={slot} />
                </li>)}
            </ListGroup>

        </div>
        );
    }
}

export default ShowForecast;
