import React from 'react';
import ShowForecast from './ShowForecast';
import { shallow } from 'enzyme';
import ShowDayForecast from '../ShowDayForecast/ShowDayForecast';

describe('<ShowForecast />', () => {
    let forecast,
        city = {
            name: 'London'
        };
    
    describe('default dom', () => {
        beforeEach(()=>{
            forecast = [];
        });
        it('should renders without crashing', () => {
            shallow(<ShowForecast city={city} forecast={forecast} />);
        });

        it('should show the city name', () => {
            const component = shallow(<ShowForecast city={city} forecast={forecast} />);
            expect(component.find('.app__header').text()).toBe('London');
        });

        it('should show call the `chooseCity` callback on search-button click', () => {
            const chooseCity = jasmine.createSpy();
            
            const component = shallow(<ShowForecast city={city} forecast={forecast} chooseCity={chooseCity} />);
            component.find('.app__header__search-icon').simulate('click');
            expect(chooseCity).toHaveBeenCalledWith();
        });
    });

    describe('days dom', () => {
        it('should render the days', () => {
            forecast = [{
                date: '2017-06-10'
            }, {
                date: '2017-06-11'
            }, {
                date: '2017-06-12'
            }];
            city = {
                name: 'London'
            };
            const component = shallow(<ShowForecast city={city} forecast={forecast} />);
            const dayForecastComponents = component.find(ShowDayForecast);
            expect(dayForecastComponents.length).toBe(3);
            expect(dayForecastComponents.nodes[0].props.dayForecast).toBe(forecast[0]);
            expect(dayForecastComponents.nodes[1].props.dayForecast).toBe(forecast[1]);
            expect(dayForecastComponents.nodes[2].props.dayForecast).toBe(forecast[2]);
        });
    });

});